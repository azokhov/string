// String.cpp : This file contains the 'main' function. Program execution begins and ends there.
// This program allows you to learn about your favorite color.

#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "Please, enter your favorite color, and press Enter" << "\n";
    string color;
    getline(cin, color);
           
    cout << "Ok, you like " << color << " color!" << "\n" << 
        "Do you know, that your favorite color has " << 
        color.length() << " letters, " << 
        "starts with a '" << color[0] << 
        "', and ends with a '" << color[color.length()-1] << "'?" << 
        "\n" << 
        "You're welcome!" << "\n";
    
    return 0;
}
